﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PZ_06_02
{
    public static class Place
    {
        public static void ZwiekszPensje(Pracownik p)
        {
            if(p.LicznaSprzedazy > 10)
            {
                

                p.Pensja += 200;
                Console.WriteLine($"{p.ToString()}  zmieniam mu pensje {p.Pensja}");
            }
        }
        public static void MniejNizDwaLata(Pracownik p)
        {
            if (p.Staz < 2)
            {
                p.Pensja -= (p.Pensja / 10);

                Console.WriteLine($"{p.ToString()}  zmieniam mu pensje bo ma  maly staz {p.Pensja}");
            }
        }
        public static void zerujSprzedaz(Pracownik p)
        {

            p.LicznaSprzedazy = 0;

            Console.WriteLine($"{p.ToString()} zeruje sprzedaz {p.LicznaSprzedazy}");
        }
    }
}
