﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PZ_06_02
{
    public delegate void pracownikD(Pracownik p);
    
    public class Pracownik
    {


        public string Imie { get; set; }
        public string Nazwisko { get; set; }
        public int Staz { get; set; }
        public int LicznaSprzedazy { get; set; }
        public double Pensja { get; set; }
        public override string ToString()
        {
            return String.Format($" Imie: {Imie} Nazwisko: {Nazwisko}  ");
        }
    }
}
