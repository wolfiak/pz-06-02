﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PZ_06_02
{

   public class Firma
    {
        List<Delegate> listaD = new List<Delegate>();
        public delegate void Dodawanie(Pracownik p);
        public delegate string Info(Pracownik s);
        public delegate string Szuk(Pracownik p);
        public delegate bool Wieksz(Pracownik p1, Pracownik p2);
        private Dodawanie pracownik;
        private EventHandler eh;
        public  event Dodawanie onEvento;
        private event EventHandler<Ea> evento2;
        public List<Pracownik> lista = new List<Pracownik>();



        public  void info(Info i)
        {
            foreach(Pracownik p in lista)
            {
                Console.WriteLine("Informacja o praconiku: "+i(p));
            }
        }
        public List<Pracownik> szukaj(Szuk i)
        {
            List<Pracownik> lip = new List<Pracownik>();
            foreach(Pracownik p in lista)
            {
                if(p.Imie == i(p))
                {
                    lip.Add(p);
                }else if (p.Nazwisko == i(p))
                {
                    lip.Add(p);
                }

            }
            return lip;
        }
        public void sortowanie(Wieksz i)
        {
            Pracownik[] p = lista.ToArray();
            Pracownik tmp = null;
            for(int m = 0; m < lista.Count; m++)
            {
                for (int n = 0; n < lista.Count-1; n++)
                {
                    if (i(p[n], p[n + 1]))
                    {
                        tmp = p[n + 1];
                        p[n + 1] = p[n];
                        p[n] = tmp;
                    }
                }
            }

            Console.WriteLine("Posortowana");
            foreach(Pracownik pr in p)
            {
                Console.WriteLine(pr);
            }
        }
        
        public void dodajMetodyPracownika(pracownikD p)
        {
            foreach(Pracownik pr in lista)
            {
                p(pr);
            }
        }
        
        public void subscribe(Dodawanie p)
        {
            listaD.Add(p);
        }
        public void unsubscribe(Dodawanie p)
        {
            foreach (Delegate item in listaD)
            {
                Dodawanie d = item as Dodawanie;
                foreach(Pracownik pr in lista)
                {
                    d(pr);
                }
              
             }
        }
        public void UsunPracownika(Pracownik p)
        {
            onEvento(lista.Find(i => i.ToString().Equals(p.ToString())));
            evento2(this, new Ea(lista.FirstOrDefault<Pracownik>((pa)=> pa.ToString() == p.ToString())));

            lista.Remove(lista.Find(i => i.ToString().Equals(p.ToString())));
           
        }
        
      /*  public void Zmianka(Pracownik p)
        {
            if (evento != null)
            {
                evento(p);
            }
        }
        */
        public void WywolajnaPracownikach(pracownikD p)
        {
            foreach (Pracownik item in lista)
            {
                p.Invoke(item);
            }
        }
        public void SzukajKowalskiego()
        {
            //List<Pracownik> sub=lista.FindAll(i => i.Nazwisko == "Kowalski");
            List<Pracownik> sub = lista.FindAll(test);
            sub.ForEach(i =>
            {
                Console.WriteLine(i.ToString());
            });
        }
        public bool test(Pracownik p)
        {
            if (p.Nazwisko == "Kowalski")
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public void szukajKowlaskich2()
        {
            //Pracownik p = new Pracownik();
            List<Pracownik> kowal = lista.FindAll(delegate (Pracownik p) {
                if (p.Nazwisko == "Kowalski")
                {
                    return true;
                }
                else
                {
                    return false;
                }

            });
            kowal.ForEach((p) => Console.WriteLine(p));
        }
        public void szukajKowalskich3()
        {
            List<Pracownik> kowal = lista.FindAll((p) => p.Nazwisko == "Kowalski");
            kowal.ForEach((k) => Console.WriteLine(k));
        }
        public void szukajImieINazwisko(string imie, string nazwisko)
        {
            List<Pracownik> sub = lista.FindAll(delegate(Pracownik i) { return imie == i.Imie && i.Nazwisko == nazwisko; });
            sub.ForEach(i =>
            {
                Console.WriteLine(i.ToString());
            });
        }
        public void wyszukanieOIlosci()
        {
            List<Pracownik> sub = lista.FindAll((i) => i.LicznaSprzedazy<10 || i.LicznaSprzedazy>200);
            sub.ForEach(i =>
            {
                Console.WriteLine(i.ToString());
            });
        }
        public void wyzszyOdPodanego(int staz)
        {
            List<Pracownik> sub = lista.FindAll((i) => i.Staz>staz);
            sub.ForEach(i =>
            {
                Console.WriteLine(i.ToString());
            });
        }
    }
}
