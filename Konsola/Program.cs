﻿using PZ_06_02;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Konsola
{
    class Program
    {
        static void Main(string[] args)
        {
            Firma f = new Firma();
            f.lista.Add(new Pracownik { Imie = "Darek", Nazwisko = "Kowalski", LicznaSprzedazy = 15, Pensja = 5000, Staz = 5 });
            f.lista.Add(new Pracownik { Imie = "Andrzej", Nazwisko = "Kowalski", LicznaSprzedazy = 20, Pensja = 7000, Staz = 8 });
            f.lista.Add(new Pracownik { Imie = "Anna", Nazwisko = "Markotna", LicznaSprzedazy = 5, Pensja = 2000, Staz = 1 });
            f.lista.Add(new Pracownik { Imie = "Kasia", Nazwisko = "Nowa", LicznaSprzedazy = 50, Pensja = 12000, Staz = 12 });
            f.lista.Add(new Pracownik { Imie = "Dawid", Nazwisko = "Przybylski", LicznaSprzedazy = 12, Pensja = 3500, Staz = 2 });
            f.lista.Add(new Pracownik { Imie = "Amadeusz", Nazwisko = "Kowalski", LicznaSprzedazy = 6, Pensja = 3000, Staz = 3 });
            f.lista.Add(new Pracownik { Imie = "Kazimierz", Nazwisko = "Pierwsz", LicznaSprzedazy = 3, Pensja = 2000, Staz = 2 });
            /*      pracownikD lol = Place.ZwiekszPensje;
                  lol += Place.zerujSprzedaz;
                  lol += Place.MniejNizDwaLata;
                  foreach(Pracownik p in f.lista)
                  {
                      lol(p);

                  }
                  f.szukajImieINazwisko("Darek", "Kowalski");
                 */
            f.subscribe(Place.MniejNizDwaLata);
            f.subscribe(Place.ZwiekszPensje);
            f.subscribe(Place.zerujSprzedaz);
            f.unsubscribe(Place.zerujSprzedaz);





            Console.WriteLine("Testowanie 7");
            f.info(ingo);

            Console.WriteLine("Testowanie ostatniego:");
            f.sortowanie((p, s) => p.Staz > s.Staz);

            Console.ReadKey();
        }

        static string ingo(Pracownik p)
        {
            return p.Imie;
        }
    }
}
